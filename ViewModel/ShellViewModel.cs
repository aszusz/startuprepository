﻿using Caliburn.Micro;

namespace ViewModels
{
    public class ShellViewModel : PropertyChangedBase
    {
        private string _title;

        public ShellViewModel(string title)
        {
            _title = title;
        }

        public string Title
        {
            get { return _title; }
            set
            {
                if (value == _title) return;
                _title = value;
                NotifyOfPropertyChange(() => Title);
            }
        }
    }
}