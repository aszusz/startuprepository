using Autofac;

namespace ViewModels
{
    public class ViewModelModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ShellViewModel>();

            builder.RegisterInstance("My Title");
        }
    }
}