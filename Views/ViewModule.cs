using Autofac;

namespace Views
{
    public class ViewModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ShellView>();
        }
    }
}